package com.example.pv207.flight_hotel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AmadeusController {

    private final AmadeusService amadeusService;

    @Autowired
    public AmadeusController(AmadeusService amadeusService) {
        this.amadeusService = amadeusService;
    }

    @GetMapping("/search-flights")
    public String searchFlights(@RequestParam String origin, @RequestParam String destination, @RequestParam String departureDate) {
        return amadeusService.searchFlights(origin, destination, departureDate);
    }

    @GetMapping("/search-by-city")
    public String searchHotelsByCity(@RequestParam String cityCode) {
        return amadeusService.searchHotelsByCity(cityCode);
    }

}
