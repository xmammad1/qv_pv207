//package com.example.pv207.mail;
//
//import org.apache.camel.ProducerTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//@Component
//public class MailSender {
//
//    private final ProducerTemplate producerTemplate;
//
//    @Autowired
//    public MailSender(ProducerTemplate producerTemplate) {
//        this.producerTemplate = producerTemplate;
//    }
//
//    public void sendMail(String body) {
//        producerTemplate.sendBody("direct:sendMail", body);
//    }
//}
