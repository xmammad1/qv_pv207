//package com.example.pv207.mail;
//
//import org.apache.camel.ProducerTemplate;
//import org.kie.api.runtime.process.WorkItem;
//import org.kie.api.runtime.process.WorkItemManager;
//import org.kie.api.runtime.process.WorkItemHandler;
//
//public class CamelWorkItemHandler implements WorkItemHandler {
//
//    private ProducerTemplate producerTemplate;
//
//    public CamelWorkItemHandler(ProducerTemplate producerTemplate) {
//        this.producerTemplate = producerTemplate;
//    }
//
//    @Override
//    public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
//        // Extract parameters
//        String message = (String) workItem.getParameter("Message");
//
//        // Send message using Camel
//        producerTemplate.sendBody("direct:sendEmail", message);
//
//        // Notify manager that work item has been completed
//        manager.completeWorkItem(workItem.getId(), null);
//    }
//
//    @Override
//    public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
//        // Called if the process is aborted
//    }
//}
