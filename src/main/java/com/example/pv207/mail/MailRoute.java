//package com.example.pv207.mail;
//
//import org.apache.camel.builder.RouteBuilder;
//import org.springframework.stereotype.Component;
//
//@Component
//public class MailRoute extends RouteBuilder {
//
//    @Override
//    public void configure() throws Exception {
//        from("direct:sendMail")
//            .to("smtp://smtp.example.com?username=user&password=secret&to=receiver@example.com&subject=Hello");
//    }
//}