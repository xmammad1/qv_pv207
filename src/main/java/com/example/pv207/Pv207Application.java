package com.example.pv207;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
public class Pv207Application {

    public static void main(String[] args) {
        SpringApplication.run(Pv207Application.class, args);
    }

}
