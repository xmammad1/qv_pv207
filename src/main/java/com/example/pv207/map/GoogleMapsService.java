package com.example.pv207.map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class GoogleMapsService {

    private final RestTemplate restTemplate;

    @Value("${google.api.key}")
    private String apiKey;

    public GoogleMapsService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String geocodeAddress(String address) {
        String baseUrl = "https://maps.googleapis.com/maps/api/geocode/json";
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .queryParam("address", address)
                .queryParam("key", apiKey);

        return restTemplate.getForObject(uriBuilder.toUriString(), String.class);
    }
}

