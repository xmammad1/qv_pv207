# Project description

This project is a multi-module Spring Boot application built with Java and Maven. It consists of several modules, each providing a specific functionality: 

1. Weather Module: This module uses the OpenWeatherMap API to fetch weather data. It exposes a REST API endpoint at /api/weather/{city}. When a GET request is made to this endpoint with a city name as a path variable, the application fetches the weather data for that city from the OpenWeatherMap API and returns it.

2. Flight and Hotel Module: This module uses the Amadeus API to fetch flight and hotel data. It exposes two REST API endpoints: /search-flights and /search-by-city. The /search-flights endpoint takes origin, destination, and departure date as parameters and returns flight offers. The /search-by-city endpoint takes a city code as a parameter and returns hotels in that city. 

3. Google Maps Module: This module uses the Google Maps API to geocode addresses. It exposes a REST API endpoint at /geocode/{address}. When a GET request is made to this endpoint with an address as a path variable, the application fetches the geocoded data for that address from the Google Maps API and returns it.
  
## Prerequisites

Before you begin, ensure you have met the following requirements:

* You have installed the latest version of Java and Maven.
* You have a Windows machine. This project may run on Linux and Mac, but it was developed and tested on Windows.

## Installing qv_pv207

To install qv_pv207, follow these steps:

1. Clone the repository: `git clone https://gitlab.fi.muni.cz/xmammad1/qv_pv207.git`
2. Install the dependencies: `mvn clean install`
3. Run the project:`mvn spring-boot:run`
4. Test the project endpoints: `http://localhost:8080/swagger-ui/index.html#/`
